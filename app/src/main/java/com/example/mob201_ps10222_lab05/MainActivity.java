package com.example.mob201_ps10222_lab05;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    ListView lv_news;
    String [] ten_loai={"Làm đẹp", "Công nghệ thông tin", "Thời trang", "Bóng đá"};
    String [] rss_loai={"https://www.24h.com.vn/upload/rss/lamdep.rss",
            "https://www.24h.com.vn/upload/rss/bongda.rss",
            "https://www.24h.com.vn/upload/rss/congnghethongtin.rss",
            "https://www.24h.com.vn/upload/rss/thoitrang.rss",
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lv_news = (ListView) findViewById(R.id.lv_news);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, ten_loai);
        lv_news.setAdapter(adapter);

        lv_news.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                Intent intent = new Intent(getApplicationContext(), TinTheoLoai.class);
                intent.putExtra("diachi_rss", rss_loai[arg2]);
                startActivity(intent);
            }
        });

    }
}

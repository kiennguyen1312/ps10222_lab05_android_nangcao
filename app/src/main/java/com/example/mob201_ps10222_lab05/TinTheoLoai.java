package com.example.mob201_ps10222_lab05;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class TinTheoLoai extends Activity {

    ArrayList<Item> items = new ArrayList<Item>();

    String diachi_rss;
    ListView lv_tintheoloai;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tin_theo_loai);

        diachi_rss = getIntent().getExtras().getString("diachi_rss");
        Toast.makeText(getApplicationContext(), diachi_rss, Toast.LENGTH_SHORT).show();

        MyAsyncTask gandulieu = new MyAsyncTask();
        gandulieu.execute();

        lv_tintheoloai = (ListView) findViewById(R.id.lv_tintheoloai);
        lv_tintheoloai.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3){
                String link = items.get(arg2).getLink();
                Intent intent = new Intent(getApplicationContext(),XemTin.class);
                intent.putExtra("link", link);
                startActivity(intent);
            }
        });
    }

    class MyAsyncTask extends AsyncTask<Void, Void, Void>{

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = ProgressDialog.show(TinTheoLoai.this, "System", "Loading...");
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                URL url = new URL(diachi_rss);
                URLConnection connection = url.openConnection();
                InputStream is = connection.getInputStream();
                items = (ArrayList<Item>) MySaxParser.xmlParser(is);

            } catch (Exception e){
                e.printStackTrace();
            } return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            dialog.dismiss();
            try{
                MyAdapter adapter = new MyAdapter(getApplicationContext(), R.layout.layout_tintheoloai_itemlv, items);
                lv_tintheoloai.setAdapter(adapter);
            } catch (Exception e){
                Log.d("title","adapter không được");
            }
        }
    }

    class MyAdapter extends ArrayAdapter<Item>{
        Context context;
        ArrayList<Item> items;

        public MyAdapter (Context context, int textViewResourceId, ArrayList<Item>objects){
            super(context, textViewResourceId, objects);
            this.context = context;
            this.items = objects;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowview = inflater.inflate(R.layout.layout_tintheoloai_itemlv, parent, false);

            TextView tv_title = (TextView) rowview.findViewById(R.id.title);
            TextView tv_description = (TextView) rowview.findViewById(R.id.description);
            TextView tv_pubdate = (TextView) rowview.findViewById(R.id.pubdate);

            tv_title.setText(items.get(position).getTittle().toString());
            tv_description.setText(items.get(position).getDescription().toString());
            tv_pubdate.setText(items.get(position).getPubdate().toString());

            return  rowview;
        }
    }
}
